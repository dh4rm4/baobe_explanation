#include "point.h"

void set_point(t_point *p_point)
{
	p_point->x = 42;
	p_point->y = 21;
}

int main(void)
{
	t_point point;

	set_point(&point);
	return (0);
}
