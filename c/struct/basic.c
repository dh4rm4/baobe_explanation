#include <stdio.h>

typedef struct Cat {
	char *color;
	char *size;
	int has_hair;
} t_cat;


void set_cat_has_hair(t_cat *my_cat)
{
	my_cat->has_hair = 1;
}

void print_infos_on_cats(t_cat first_cat, t_cat scd_cat)
{
	if (first_cat.has_hair == 1)
		printf("My %s cat have %s hair\n", first_cat.size, first_cat.color);
	if (scd_cat.has_hair == 1)
		printf("My %s mao have %s hair", scd_cat.size, scd_cat.color);
}


int main()
{
	int n;
	t_cat meilv;
	t_cat bao;

	meilv.color = "golden";
	meilv.size = "so tiny";
	set_cat_has_hair(&meilv);

	bao.color = "pink";
	bao.size = "xiao xiao xiao";
	bao.has_hair = 1;


	print_infos_on_cats(meilv, bao);
	return (0);
}
